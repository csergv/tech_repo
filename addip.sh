## Script for adding extra IP addresses on dedicated servers with CentOS Linux OS
#!/bin/bash

interface="eth1"                                # Physical public network interface
path="/etc/sysconfig/network-scripts/"          # Path to network configuration files
red="\033[0;31m"                                # Red
green="\033[0;32m"                              # Green
blue="\033[0;34m"                               # Blue
nc="\033[0m"                                    # No Color

cd ${path}
number=$(ls -1|grep "ifcfg\-${interface}"|wc -l)
read -r -p "$(echo -e ${blue}Enter a new IP address \(for example 192.168.0.1\): ${nc})" newip
if /bin/echo ${newip} | /bin/grep -E -o "\b((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b" >/dev/null
then
echo -e "${green}The IP address \"${newip}\" is valid!${nc}"
else
echo -e "${red}The IP address \"${newip}\" is NOT valid! Please try again!${nc}"
exit 1
fi
read -r -p "$(echo -e ${blue}Enter a netmask [24-30]: ${nc})" netmask
if /bin/echo ${netmask} | /bin/grep -E '2[4-9]|30' >/dev/null
then 
echo -e "${green}The netmask \"/${netmask}\" ($(ipcalc --netmask ${newip}/${netmask} | awk -F "=" '{print $2}')) is valid!${nc}"
else  
echo -e "${red}The value of netmask \"/${netmask}\" is incorrect or out of range. Please try again!${nc}"
exit 1
fi

read -r -p  "$(echo -e ${blue}Do you want to create a config file \"ifcfg-${interface}:${number}\" for new IP address \"$newip/$netmask\"? [y/n]: ${nc})" answer1
case ${answer1} in
[yY][eE][sS]|[yY])
touch "ifcfg-${interface}:${number}"
echo DEVICE=${interface}:${number} >> ifcfg-${interface}:${number}
echo BOOTPROTO=static >> ifcfg-${interface}:${number}
echo ONBOOT=yes >> ifcfg-${interface}:${number}
echo IPADDR=${newip} >> ifcfg-${interface}:${number}
echo NETMASK=$(ipcalc --netmask ${newip}/${netmask} | awk -F "=" '{print $2}') >> ifcfg-${interface}:${number}
echo -e "${green}The config file \"ifcfg-${interface}:${number}\" for IP address \"${newip}/${netmask}\" has been added.${nc}" 
;;
[nN][oO]|[nN])  echo -e "${red}You refused to create a config file for new IP address!${nc}" ; exit 0
;;
*) echo -e "${red}Invalid input. Please try again!${nc}" ; exit 1
;;
esac

read -r -p  "$(echo -e ${blue}Do you want to restart network service to apply the changes? [y/n]: ${nc})" answer2
case ${answer2} in
[yY][eE][sS]|[yY])
echo -e "${green}Network service restarting ...${nc}"
service network restart >/dev/null
if /sbin/ip -family inet address show ${interface} | /bin/grep ${newip} >/dev/null
then
echo -e ""
echo -e "${green}===================================================================================================${nc}"
echo -e "$The IP address \"${newip}/${netmask}\" has been added to virtual interface \"${interface}:${number}\" successfully!" 
else
echo -e "${green}===================================================================================================${nc}"
echo -e "${red}The IP address \"${newip}\" has not been added! Please check!!!${nc}"
exit 1
fi
;;
[nN][oO]|[nN])  echo -e "${red}Please restart network service manually!${nc}" ; exit 0
;;
*) echo -e "${red}Invalid input... Please try again!${nc}" ; exit 1
;;
esac

echo -e "${green}===================================================================================================${nc}"
echo -e "${green}The current list of IP addresses on the server:${nc}"
/sbin/ip -family inet address show ${interface} | /bin/grep "inet"
echo -e "${green}===================================================================================================${nc}"
echo -e "${green}Ping test:${nc}"
ping -c 3 ${newip}
echo -e "${green}===================================================================================================${nc}"
exit 0